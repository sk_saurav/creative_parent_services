/*
 * Copyright Reserved by Saurav from 2021.
 */

package com.creative.banner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.creative.common", "com.creative.banner"})
public class BannerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BannerServiceApplication.class, args);
	}

}

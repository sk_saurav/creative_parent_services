/*
 * Copyright Reserved by Saurav from 2021.
 */

package com.creative.banner.controller;

import com.creative.common.model.ResultVO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * The type Banner controller.
 */
public class BannerController {
    /**
     * Gets banner infos.
     *
     * @param modelName the model name
     * @return the banner infos
     */
    @GetMapping("/banner/{modelName}")
    public ResponseEntity<ResultVO<String>> getBannerInfos(@PathVariable String modelName) {
        ResultVO resultVO = new ResultVO();
        return new ResponseEntity<>(resultVO, HttpStatus.OK);
    }
}

/*
 * Copy right from Saurav kumar
 */

package com.creative.common.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

@Component
public class RequestHeaderInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response   , Object handler) throws Exception {
        /*if (StringUtils.isEmpty(request.getHeader("student-auth-key"))) {
            throw new InvalidHeaderFieldException("Invalid request");
        }*/
        return super.preHandle(request, response, handler);
    }
}

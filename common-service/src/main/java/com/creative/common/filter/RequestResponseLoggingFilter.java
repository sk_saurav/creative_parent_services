/*
 * Copy right from Saurav kumar
 */

package com.creative.common.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(1)
public class RequestResponseLoggingFilter implements Filter {
    private static Logger logger = LoggerFactory.getLogger(RequestResponseLoggingFilter.class);

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest ;
        HttpServletResponse res = (HttpServletResponse) servletResponse ;
        logger.info("Logging Filter Request  {} : {}", req.getMethod(),
                req.getRequestURI());
        filterChain.doFilter(req, res);
        logger.info("Logging Filter Response :{}", res.getContentType());
    }

}

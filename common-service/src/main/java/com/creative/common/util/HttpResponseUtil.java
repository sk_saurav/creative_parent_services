/*
 * Copy right from Saurav kumar
 */

package com.creative.common.util;

import org.springframework.http.HttpStatus;

public class HttpResponseUtil {

    public static HttpStatus getHttpStatus(String code) {
        HttpStatus statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
        if (null != code) {
            switch (code) {
                case IResultCode.SUCCESS_CODE:
                    statusCode = HttpStatus.OK;
                    return statusCode;
                case IResultCode.INVALID_TEMPLATE_ID:
                case IResultCode.MODIFIER_CANT_EMPTY:
                case IResultCode.SOLUTION_UNSUPPORTED:
                case IResultCode.TEMPLATE_ID:
                    statusCode = HttpStatus.BAD_REQUEST;
                    return statusCode;
            }
            return statusCode;
        } else {
            return statusCode;
        }
    }
}

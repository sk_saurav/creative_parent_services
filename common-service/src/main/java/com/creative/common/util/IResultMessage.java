/*
 * Copy right from Saurav kumar
 */

package com.creative.common.util;

/**
 * The interface Result message.
 */
public interface IResultMessage {
	/**
	 * The constant INVALID_TEMPLATE_ID.
	 */
	String INVALID_TEMPLATE_ID = "Template doesn't exist.";
	/**
	 * The constant TEMPLATE_ID.
	 */
	String TEMPLATE_ID = "TemplateID can't be null.";
	/**
	 * The constant SOLUTION_UNSUPPORTED.
	 */
	String SOLUTION_UNSUPPORTED = "Solution doesn't support.";
	/**
	 * The constant SUCCESS_CODE.
	 */
	String SUCCESS_CODE = "Operation Completed successfully.";
	/**
	 * The constant MODIFIER_CANT_EMPTY.
	 */
	String MODIFIER_CANT_EMPTY = "Modifier can't be empty.";
	/**
	 * The constant REQUEST_BODY_EMPTY.
	 */
	String REQUEST_BODY_EMPTY = "Request body can't be empty.";
	/**
	 * The constant INVALID_REQUEST.
	 */
	String INVALID_REQUEST = "Invalid request body.";
	String COMPLETED = "All requests are completely processed!";
}

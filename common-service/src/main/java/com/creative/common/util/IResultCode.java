/*
 * Copy right from Saurav kumar
 */

package com.creative.common.util;

/**
 * The interface Result code.
 */
public interface IResultCode {
    /**
     * The constant DELETE_SUCCESS.
     */
    String DELETE_SUCCESS = "200001";

    /**
     * The constant INVALID_TEMPLATE_ID.
     */
    String INVALID_TEMPLATE_ID = "300001";

    /**
     * The constant TEMPLATE_ID.
     */
    String TEMPLATE_ID = "300002";

    /**
     * The constant SOLUTION_UNSUPPORTED.
     */
    String SOLUTION_UNSUPPORTED = "300003";

    /**
     * The constant SUCCESS_CODE.
     */
    String SUCCESS_CODE = "000000";

    /**
     * The constant MODIFIER_CANT_EMPTY.
     */
    String MODIFIER_CANT_EMPTY = "300004";

    /**
     * Request body can't be empty.
     */
    String REQUEST_BODY_EMPTY = "300005";

    /**
     * The constant INVALID_REQUEST.
     */
    String INVALID_REQUEST = "300006";

    /**
     * The constant COMPLETED.
     */
    String COMPLETED = "300007";
}

/*
 * Copyright Reserved by Saurav from 2021.
 */

package com.creative.common.exception;


public class IdNotFoundException extends RuntimeException {
    public IdNotFoundException(String msg) {
        super(msg);
    }
}

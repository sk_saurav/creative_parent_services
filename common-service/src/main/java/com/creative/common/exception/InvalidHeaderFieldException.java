/*
 * Copyright Reserved by Saurav from 2021.
 */

package com.creative.common.exception;

public class InvalidHeaderFieldException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private String message;

    /**
     * Constructs a new runtime exception with {@code null} as its
     * detail message.  The cause is not initialized, and may subsequently be
     * initialized by a call to {@link #initCause}.
     */
    public InvalidHeaderFieldException(String message) {
        this.setMessage(message);
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

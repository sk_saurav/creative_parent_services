package com.creative.common.aop.advice;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class CustomLoggingAdvice {
    /**
     * custom log to track the process flow.
     */
    private static final Logger logger = LoggerFactory.getLogger(CustomLoggingAdvice.class);

    @Pointcut("execution(* com.learn.spring.mybatisservice.*.*.*(..))")
    public void loggingPointCuts() {

    }

    @Around("loggingPointCuts()")
    public Object addCustomLoggingAdvice(@NotNull ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        ObjectMapper objectMapper = new ObjectMapper();
        String targetName = proceedingJoinPoint.getSignature().getName();
        Object[] fields = proceedingJoinPoint.getArgs();
        logger.info("Entering into {}, with args {}.", targetName, objectMapper.writeValueAsString(fields));
        Object proceed = proceedingJoinPoint.proceed();
        logger.info("Returning from {}, with response {}.", targetName, objectMapper.writeValueAsString(proceed));
        return proceed;
    }
}

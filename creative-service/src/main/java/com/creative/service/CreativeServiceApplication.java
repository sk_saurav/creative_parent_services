/*
 * Copyright Reserved by Saurav from 2021.
 */

package com.creative.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CreativeServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CreativeServiceApplication.class, args);
	}

}
